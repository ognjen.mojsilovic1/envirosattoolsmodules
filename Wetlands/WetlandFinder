// adapted from:
// https://gis.stackexchange.com/questions/327232/calculate-water-frequency-in-google-earth-engine-based-on-time-series

var nz = ee.FeatureCollection('USDOS/LSIB_SIMPLE/2017')
.filter(ee.Filter.eq('country_na', 'New Zealand'));

var S1coll = ee.ImageCollection('COPERNICUS/S1_GRD')
//.filterBounds(nz)
.filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VH'))
.filter(ee.Filter.eq('resolution_meters', 10))
.filter(ee.Filter.eq('instrumentMode', 'IW'))
.filter(ee.Filter.eq('orbitProperties_pass', 'ASCENDING'))
.select('VH')
.filterDate('2019-01-01', '2019-12-31');

var S1_STDEV = S1coll.select(['VH']).reduce(ee.Reducer.stdDev());//.clip(nz);

function maskS2scl(image) {
  var scl = image.select('SCL');
  var mask_shadows = scl.gte(4);
  var mask_clouds = scl.lte(6);
  var mask = mask_shadows.eq(mask_clouds);
  return image.updateMask(mask);
}

var S2 = ee.ImageCollection('COPERNICUS/S2_SR')
.filterDate('2019-01-01', '2019-12-31')
.filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 20))
.map(maskS2scl);

var decision_tree = function(image){
  var mndwi = image.normalizedDifference(['B3', 'B8']);
  var evi = image.expression(
    '(NIR - RED)/(NIR+6*RED-7.5*BLUE+1)*2.5', {
      'BLUE': image.select('B2'),
      'RED': image.select('B4'),
      'NIR': image.select('B8'),
    });
  var ndvi = image.normalizedDifference(['B8', 'B4']);
  var lswi = image.normalizedDifference(['B8A', 'B11']);
  var water = evi.lt(0.1).and(mndwi.gt(evi).or(mndwi.gt(ndvi))).rename('water');
  var vegetation = evi.gte(0.1).and (ndvi.gte(0.2)).and (lswi.gt(0)).rename('vegetation');
  return water.addBands(vegetation).copyProperties(image);  
};

var result = S2.map(decision_tree);
var freq = result.sum().divide(result.count());

// var VisParamWar = {"bands":["water"], "min":0, "max":1};
// var VisParamVeg = {"bands":["vegetation"], "min":0, "max":1};

var water = freq.select('water')
var vegetation = freq.select('vegetation')
var wetland = water
//.updateMask(S1_STDEV.gt(5))
.updateMask(vegetation.gt(0.1));

// Map.addLayer(wetland, {min: 0, max: 1, palette: ['white', 'blue']},'Wetland');
// Map.addLayer(water, {min: 0, max: 1, palette: ['white', 'blue']},'Water freq');
// Map.addLayer(vegetation, {min: 0, max: 1, palette: ['white', 'green']},'Vegetation freq');
// Map.addLayer(S1_STDEV, {min: 2, max: 10, palette: ['white', 'yellow', 'blue', 'black']}, 'S1_STDEV');

// Map.setCenter(175.27, -41.25, 13);
// Map.setCenter(174.28871527120518,-35.59441243985227, 13);
// Map.setCenter(172.70567125592947,-43.56896092462822, 15);

// Ben's additions start here (commented a couple of things above including S1 layer clips but that's it)
//    Loosely based on this example:
//      https://clubgis.net/cyclone-amphan-seen-with-google-earth-engine/
var lcdb_wetlands = ee.FeatureCollection('users/jollyb/lcdb/v5_wetland18');

var maplayers = [
  {label: 'None (clear)', value: ui.Map.Layer(null, null, 'Null')},
  {label: 'LCDBv5 Wetlands', value: ui.Map.Layer(lcdb_wetlands, null, 'LCDBv5 Wetlands')}
];

var addlayer = function(img, visparams, name) {
  maplayers.push(
    { label: name, value: ui.Map.Layer(img, visparams, name) }
  )
}

// Extra images to see what might help find wetlands:
//    Bog standard mosaic (Dec/Jan 18/19)
var composite = ee.ImageCollection('COPERNICUS/S2_SR')
.filterDate('2018-12-01', '2019-01-31')
.filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 20))
.map(maskS2scl).median();

//    Lower-quartile of S1 stack (find standing water that hangs around a bit)
var s1lq = S1coll.select(['VH']).reduce(ee.Reducer.percentile([25]));

//    Water * Veg Freqs = Wetlands? (hint: sort of) - range is 0 to 0.1 ish
var wetland_factor = water.multiply(vegetation);

//    Try using that to mask the S1 LQ image
var s1lq_masked = s1lq.updateMask(wetland_factor.gt(0.01));

//    What about subtracting S1 stdev from the LQ (low LQ means more 
//    chance of standing water, higher STDEV means larger fluctuations,
//    more negative s1_proto might indicate more likely wetlands)
var s1_proto = s1lq.subtract(S1_STDEV);

//    As s1lq_masked, but the STDEV product
var s1_std_masked = S1_STDEV.updateMask(wetland_factor.gt(0.01));

// Add all these layers to our list for the dropdowns
addlayer(wetland_factor, {min: 0, max: 0.1, palette: ['white', 'blue']},'Wetland Factor');
addlayer(water, {min: 0, max: 1, palette: ['white', 'blue']},'S2 Water Frequency');
addlayer(vegetation, {min: 0, max: 1, palette: ['white', 'green']},'S2 Vegetation Frequency');
addlayer(composite, {bands: ['B4', 'B3', 'B2'], max: 0.3*10000}, 'S2 Composite')
addlayer(S1_STDEV, {min: 2, max: 10, palette: ['white', 'yellow', 'blue', 'black']}, 'S1 Std. Dev.');
addlayer(s1_std_masked, {min: 2, max: 10, palette: ['white', 'yellow', 'blue', 'black']}, 'S1 Stdev Masked');
addlayer(s1lq, {min:-40, max:-10, palette: ['blue', 'green', 'yellow', 'white']}, 'S1 LQ');
addlayer(s1_proto, {min:-40, max:-10, palette: ['blue', 'blue', 'yellow', 'white']}, 'S1 Proto');

// Dropdown selection widgets for users to select layers
var leftSelect = ui.Select({
  items: maplayers,
  placeholder: 'Left Image',
  onChange: function(maplayer){
    layerLabelLeft.setValue(maplayer.get('name'));
    leftMap.layers().set(0, maplayer);
  },
  style:{padding: '0px 0px 0px 10px', stretch: 'horizontal'}
});
var rightSelect = ui.Select({
  items: maplayers,
  placeholder: 'Right Image',
  onChange: function(maplayer){
    layerLabelRight.setValue(maplayer.get('name'));
    rightMap.layers().set(0, maplayer);
  },
  style:{padding: '0px 10px 0px 00px', stretch: 'horizontal'}
});

// Create a left-hand panel to hold text/buttons 
var leftPanel = ui.Panel({
    layout: ui.Panel.Layout.flow('vertical'),
    style: {width: '300px'}
  });
  
// Title for text in left-hand panel
var title = ui.Label('Wetland Finder');
title.style().set('color', 'blue');
title.style().set('fontWeight', 'bold');
title.style().set({
  fontSize: '20px',
  padding: '10px'
});

// Description text in left-hand panel
var desc = ui.Label("This App is intended to help identify \
  potential wetland locations in NZ. Choose layers to view using \
  the drop-down selectors below. Buttons will 'jump' the display \
  to different NZ locations.");
desc.style().set({ fontSize: '16px', padding: '0px 10px'});

// Labels to display layer names
var layerLabelLeft = ui.Label('Left Layer');
var layerLabelRight = ui.Label('Right Layer');

// Builder function for Map objects
var buildMap = function(label){
  var map = ui.Map().setOptions({mapTypeId: "HYBRID"});
  map.setControlVisibility({ layerList : false, zoomControl: false, mapTypeControl : true, fullscreenControl : false });
  map.style().set('cursor', 'hand');
  map.add(label);
  return map;
}

// Create two Map objects and link them (for our split view)
var leftMap = buildMap(layerLabelLeft);
var rightMap = buildMap(layerLabelRight)
var linkedMaps = ui.Map.Linker([leftMap, rightMap]);

// Create a SplitPanel which holds the linked maps side-by-side.
var mainSplitPanel = ui.SplitPanel({
  firstPanel: linkedMaps.get(0),
  secondPanel: linkedMaps.get(1),
  orientation: 'horizontal',
  wipe: true,
  style: {stretch: 'both'}
});

// Add a location button to leftPanel (hard-coded)
var addlocationButton = function(name, lon, lat, zoom){
  leftPanel.add(
      ui.Button({
        label: name,
        onClick: function(){
          leftMap.setCenter(lon, lat, zoom)
        }
    })
  );
};

leftPanel.add(title);
leftPanel.add(desc);
leftPanel.add(ui.Panel([leftSelect, rightSelect], ui.Panel.Layout.flow('horizontal')));
addlocationButton('Kaimaumau',173.281, -34.944, 13);
addlocationButton('Hampton Downs',175.1322, -37.342, 13);
addlocationButton('Lake Wairarapa',175.27, -41.25, 13);
addlocationButton('Haast',169.077, -43.874, 13);

ui.root.clear();
ui.root.insert(0, leftPanel);
ui.root.insert(1, mainSplitPanel);

leftMap.setCenter(175.27, -41.25, 13);

// leftMap.layers().set(1, ui.Map.Layer(lcdb_wetlands));
// leftMap.layers().set(0, ui.Map.Layer(water, {min: 0, max: 1, palette: ['white', 'blue']},'Water freq'));
// rightMap.layers().set(0, ui.Map.Layer(S1_STDEV, {min: 2, max: 10, palette: ['white', 'yellow', 'blue', 'black']}, 'S1_STDEV'));

  
  